from django.shortcuts import render, get_object_or_404, redirect
from django.contrib.auth.decorators import login_required
from .models import Project
from .forms import ProjectForm

# Create your views here.


@login_required
def list_projects(request):
    context = {"project_objects": Project.objects.filter(owner=request.user)}
    return render(request, "projects/list_projects.html", context)


@login_required
def show_project(request, id):
    new_project = get_object_or_404(Project, id=id)
    context = {"project_object": new_project}
    return render(request, "projects/show_project.html", context)


@login_required
def create_project(request):
    if request.method == "POST":
        form = ProjectForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect("list_projects")
    else:
        form = ProjectForm()
    context = {"form": form}
    return render(request, "projects/create.html", context)
